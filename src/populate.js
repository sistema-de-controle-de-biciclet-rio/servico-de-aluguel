const fs = require("node:fs/promises");
const Datasource = require("./datasource.js");

const [funcionarioRepository, cartaoDeCreditoRepository, ciclistaRepository, aluguelRepository] = ["funcionario", "cartaoDeCredito", "ciclista", "aluguel"].map(nome => {
    return Datasource.getRepository(nome);
});

module.exports = async () => {
    const { funcionarios, cartoesDeCredito, ciclistas, alugueis } = JSON.parse((await fs.readFile("./amostra-aluguel.json", { encoding: "utf-8" })));

    funcionarios.forEach(funcionario => {
        funcionarioRepository.salvar(funcionario);
    });

    cartoesDeCredito.forEach(cartaoDeCredito => {
        cartaoDeCreditoRepository.salvar(cartaoDeCredito);
    });

    ciclistas.forEach(ciclista => {
        ciclistaRepository.salvar(ciclista);
    });

    const dataAtual = new Date();
    dataAtual.setHours(dataAtual.getHours() - 2);

    await aluguelRepository.salvar({ trancaFim: -1, horaFim: "", ...alugueis[0], horaInicio: new Date() });
    await aluguelRepository.salvar({ trancaFim: -1, horaFim: "", ...alugueis[1], horaInicio: dataAtual });
    await aluguelRepository.salvar({  ...alugueis[2], horaFim: new Date(), horaInicio: dataAtual });

};