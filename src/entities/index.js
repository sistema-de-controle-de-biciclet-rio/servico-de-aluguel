const Ciclista = require("./ciclista.entity.js");
const CartaoDeCredito = require("./cartaoDeCredito.entity.js");
const Funcionario = require("./funcionario.entity.js");

module.exports = { 
    Ciclista,
    CartaoDeCredito,
    Funcionario
}