module.exports = class Ciclista {
    
    static ciclistaId = 0;

    static Status = {
        ATIVO: 'ATIVO',
        INATIVO: 'INATIVO',
        AGUARDANDO_CONFIRMACAO: 'AGUARDANDO_CONFIRMACAO'
    };
    
    constructor({ nome, nascimento, cpf, passaporte, nacionalidade, email, urlFotoDocumento, senha, dataAtivacao = null, bicicleta = null, status = Ciclista.Status.AGUARDANDO_CONFIRMACAO }) {
        //Ensira todas os dados necessários para a model...
        this._id = ++Ciclista.ciclistaId;
        this._nome = nome;
        this._nascimento = nascimento;
        this._cpf = cpf;
        this._passaporte = passaporte;
        this._nacionalidade = nacionalidade;
        this._email = email;
        this._urlFotoDocumento = urlFotoDocumento;
        this._senha = senha;
        this._status = status;
        this._dataAtivacao = dataAtivacao;
        this._bicicleta = bicicleta;
    }

    getId() {
        return this._id;
    }

    getDados(){
        return {
            nome: this._nome,
            nascimento: this._nascimento,
            cpf: this._cpf,
            passaporte: this._passaporte,
            nacionalidade: this._nacionalidade,
            email: this._email,
            urlFotoDocumento: this._urlFotoDocumento,
            id: this._id,
            status: this._status,
            dataAtivacao: this._dataAtivacao,
        }
    }

    getStatus(){
        return this._status;
    }

    ativar(){
        this._status = Ciclista.Status.ATIVO;
        this._dataAtivacao = new Date();
    }

    desativar(){
        this._status = Ciclista.Status.INATIVO;
        this._dataAtivacao = null;
    }

    podeAlugarBicicleta = () => {
        return this._dataAtivacao !== null && this._status === "CONFIRMADO" && this._bicicleta === null;
    }

    enviarEmail = async (axios, assunto, mensagem) => {
        await axios.post(`${process.env.EXTERNO_URL}/enviarEmail`, { email: this._email, assunto, mensagem });
    }
}

//* convenção _ antes de atributo
//* contrustor parametros em OBJETO
