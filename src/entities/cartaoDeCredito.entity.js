module.exports = class CartaoDeCredito {
    static cartaoDeCreditoId = 0;
    
    constructor({nomeTitular, numero, validade, cvv, ciclistaId}) {
        //Ensira todas os dados necessários para a model...
        this._nomeTitular = nomeTitular;
        this._numero = numero;
        this._validade = validade;
        this._cvv = cvv;
        this._id = ++CartaoDeCredito.cartaoDeCreditoId;
        this._ciclistaId = ciclistaId;
    }

    getDados(){
        return {
            nomeTitular: this._nomeTitular,
            numero: this._numero,
            validade: this._validade,
            cvv: this._cvv,
            id: this._id,
            ciclistaId: this._ciclistaId
        }
    }

    getId() {
        return this._id;
    }

    getCiclistaId() {
        return this._ciclistaId;
    }


    /**
     * Toda regra de negócio deve estar encapsulada na model. 
     */
}

// "nomeTitular": "string",
//     "numero": "40285701769787451404708",
//         "validade": "2024-05-24",
//             "cvv": "4904"