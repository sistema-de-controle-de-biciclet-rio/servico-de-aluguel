module.exports = class Aluguel {
    static aluguelId = 0;

    constructor({ ciclista, trancaInicio, trancaFim = -1, horaInicio = "", horaFim = "", bicicleta, cobranca, status = "EM_ANDAMENTO" }) {
        this._id = ++Aluguel.aluguelId;
        this._ciclista = ciclista;
        this._trancaInicio = trancaInicio;
        this._trancaFim = trancaFim;
        this._bicicleta = bicicleta;
        this._horaInicio = horaInicio;
        this._horaFim = horaFim;
        this._cobranca = cobranca;
        this._status = status;
    }

    getId = () => {
        return this._id;
    }

    getCiclista = () => {
        return this._ciclista;
    }

    getCobranca = () => {
        return this._cobranca;
    }

    getValor = () => {
        return this._valor;
    }
    
    getDados = () => {
        return {
            ciclista: Number.parseInt(this._ciclista),
            trancaInicio: Number.parseInt(this._trancaInicio),
            trancaFim: this._trancaFim,
            horaInicio: this._horaInicio,
            horaFim: this._horaFim,
            bicicleta: this._bicicleta,
            cobranca: this._cobranca
        }
    }

    dadosDaDevolucao = () => {
        return {
            "Bicicleta devolvida": this._bicicleta,
            "Horário da devolução": this._horaFim,
            "Tranca": this._trancaFim,
            "Valor": this._valor
        }
    }
    
    cobrarValorExtra = async (axios, valorExtra) => {
        const response = await axios.post(`${process.env.EXTERNO_URL}/cobranca`, { ciclista: this._ciclista, valor: valorExtra });

        if (!response.success) {
            await axios.post(`${process.env.EXTERNO_URL}/filaCobranca`, { ciclista: this._ciclista, valor: valorExtra });
        }
    }

    meiasHorasExcedidas = () => {
        return Math.floor((new Date() - this._horaInicio) / (1000 * 60 * 30));
    }
}