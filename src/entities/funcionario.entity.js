module.exports = class Funcionario {

    static funcionarioId = 0;

    constructor({ senha, confirmacaoSenha, email, nome, idade, funcao, cpf }) {
        //Ensira todas os dados necessários para a model...
        this._senha = senha;
        this._confirmacaoSenha = confirmacaoSenha;
        this._email = email;
        this._nome = nome;
        this._idade = idade;
        this._funcao = funcao;
        this._cpf = cpf;
        this._id = ++Funcionario.funcionarioId;
    }

    getDados(){
        return {
            senha: this._senha,
            confirmacaoSenha: this._confirmacaoSenha,
            email: this._email,
            nome: this._nome,
            idade: this._idade,
            funcao: this._funcao,
            cpf: this._cpf,
            id: this._id
        }
    }

    getId(){
        return this._id;
    }

    
}

//* convenção _ antes de atributo
//* contrustor parametros em OBJETO
