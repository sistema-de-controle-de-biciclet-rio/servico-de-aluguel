const app = require("./app.js");
const populate = require("./populate.js");
require("dotenv").config();

populate().then(() => {
    const port = process.env.APP_PORT;
    
    app.listen(port, () => {
        console.log(`Listening on port ${port}`);
    });
});