const Ciclista = require("../entities/funcionario.entity");

module.exports = class FuncionarioController {

    constructor({ datasource }) {
        // Não esqueça de passar qualquer dependência via construtor da classe...
        this.datasource = datasource;
    }

    postFuncionario = async (request, response) => {
        const { senha, confirmacaoSenha, email, nome, idade, funcao, cpf } = request.body;

        if (
            !senha ||
            !confirmacaoSenha ||
            !email ||
            !nome ||
            !idade ||
            typeof idade !== "number" ||
            !funcao ||
            !cpf
        ) {
            return response.status(422).json({ mensagem: "Dados inválidos", codigo: 422 });
        }

        if (senha !== confirmacaoSenha) {
            return response.status(422).json({ mensagem: "Senhas não conferem", codigo: 422 });
        }

        if (funcao !== "administrativo" && funcao !== "reparador") {
            return response.status(422).json({ mensagem: "Função inválida: Deve ser 'administrativo' ou 'reparador'", codigo: 422 });
        }

        const emailRegex = /^[^\s@]+@[^\s@]+$/;

        if (!emailRegex.test(email)) {
            return response.status(422).json({ mensagem: "Email inválido", codigo: 422 });
        }

        const funcionarioRepository = this.datasource.getRepository("funcionario");

        const emailExiste = await funcionarioRepository.buscarTodos({ email: email });

        if (emailExiste.length > 0) {
            return response.status(422).json({ mensagem: "Email já cadastrado", codigo: 422 });
        }

        const funcionarioSalvo = await funcionarioRepository.salvar({ senha, confirmacaoSenha, email, nome, idade, funcao, cpf });

        return response.status(200).json(funcionarioSalvo.getDados());
    };

    getFuncionarioById = async (request, response) => {
        const id = request.params.idFuncionario;

        if (!id) {
            return response.status(422).json({ mensagem: "Dados inválidos", codigo: 422 });
        }

        const funcionarioRepository = this.datasource.getRepository("funcionario");

        const funcionario = funcionarioRepository.buscarPorId(id);

        if (!funcionario) {
            return response.status(404).json({ mensagem: "Não encontrado", codigo: 404 });
        }

        return response.status(200).json(funcionario.getDados());
    };

    getAllFuncionarios = async (request, response) => {
        const funcionarioRepository = this.datasource.getRepository("funcionario");

        const funcionarios = funcionarioRepository.buscarTodos();

        return response.status(200).json(funcionarios.map(funcionario => funcionario.getDados()));
    }

    putFuncionario = async (request, response) => {
        const id = request.params.idFuncionario;
        const { senha, confirmacaoSenha, email, nome, idade, funcao } = request.body;

        if (
            !senha ||
            !confirmacaoSenha ||
            !email ||
            !nome ||
            !idade ||
            typeof idade !== "number" ||
            !funcao
        ) {
            return response.status(422).json({ mensagem: "Dados inválidos", codigo: 422 });
        }

        if (senha !== confirmacaoSenha) {
            return response.status(422).json({ mensagem: "Senhas não conferem", codigo: 422 });
        }

        if (funcao !== "administrativo" && funcao !== "reparador") {
            return response.status(422).json({ mensagem: "Função inválida: Deve ser 'administrativo' ou 'reparador'", codigo: 422 });
        }

        const funcionarioRepository = this.datasource.getRepository("funcionario");

        const emailRegex = /^[^\s@]+@[^\s@]+$/;

        if(email){
            if (!emailRegex.test(email)) {
                return response.status(422).json({ mensagem: "Email inválido", codigo: 422 });
            }
            
            const emailExiste = await funcionarioRepository.buscarTodos({ email: email });
            
            if (emailExiste.length > 0) {
                return response.status(422).json({ mensagem: "Email já cadastrado", codigo: 422 });
            }
        }

        const funcionario = funcionarioRepository.buscarPorId(id);

        if (!funcionario) {
            return response.status(404).json({ mensagem: "Funcionario não encontrado", codigo: 404 });
        };

        const funcionarioAtualizado = { senha, confirmacaoSenha, email, nome, idade, funcao };

        const funcionarioSalvo = await funcionarioRepository.atualizar(id, funcionarioAtualizado);
        
        return response.status(200).json(funcionarioSalvo.getDados())
    }

    deleteFuncionario = async (request, response) => {
        const id = request.params.idFuncionario;

        if (!id) {
            return response.status(422).json({ mensagem: "Dados inválidos", codigo: 422 });
        }

        const funcionarioRepository = this.datasource.getRepository("funcionario");

        const deletado = funcionarioRepository.deletar(id);

        if (!deletado) {
            return response.status(404).json({ mensagem: "Funcionario não encontrado", codigo: 404 });
        } else {
            return response.status(200).json({ mensagem: "Funcionario deletado com sucesso." });
        }
    }

}