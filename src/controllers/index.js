const CiclistaController = require("./ciclista.controller.js");
const CartaoDeCreditoController = require("./cartaoDeCredito.controller.js");
const FuncionarioController = require("./funcionario.controller.js");
const AluguelController = require("./aluguel.controller.js");

module.exports = {
    CiclistaController,
    CartaoDeCreditoController,
    FuncionarioController,
    AluguelController
}