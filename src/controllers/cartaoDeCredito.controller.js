const Ciclista = require("../entities/ciclista.entity");

module.exports = class CartaoDeCreditoController {
    
    constructor({ datasource }) {
        // Não esqueça de passar qualquer dependência via construtor da classe...
        this.datasource = datasource;
    }

    getCartaoDeCreditoByCiclistaId = (request, response) => {
        const ciclistaId = parseInt(request.params.idCiclista, 10);
        
        if(!ciclistaId){
            return response.status(422).json({mensagem: "Id inválido", codigo: 422});
        }

        const cartaoDeCreditoRepository = this.datasource.getRepository("cartaoDeCredito")

        const cartaoDeCredito = cartaoDeCreditoRepository.buscarTodos({ ciclistaId: ciclistaId })

        if (cartaoDeCredito.length === 0){
            return response.status(404).json({mensagem: "Cartão de crédito não encontrado", codigo: 404});
        }

        return response.status(200).json(cartaoDeCredito.map(cartao => cartao.getDados()));

    }

    putCartaoDeCredito = (request, response) => {
        const ciclistaId = parseInt(request.params.idCiclista, 10);
        const cartaoAtualizado = request.body;

        if(!ciclistaId){
            return response.status(404).json({mensagem: "Id inválido", codigo: 404});
        }

        const { nomeTitular, numero, validade, cvv } = cartaoAtualizado;

        if(
            !cartaoAtualizado || 
            !nomeTitular ||
            !numero ||
            !validade ||
            !cvv
        ){  
            return response.status(422).json({mensagem: "Dados inválidos", codigo: 422});
        }
        
        const ciclistaRepository = this.datasource.getRepository("ciclista");
        const ciclista = ciclistaRepository.buscarPorId(ciclistaId);

        if(ciclista?.getStatus() !== Ciclista.Status.ATIVO){
            return response.status(403).json({mensagem: "Ciclista inativo", codigo: 403});
        }

        const cartaoDeCreditoRepository = this.datasource.getRepository("cartaoDeCredito")
        const cartaoDeCredito = cartaoDeCreditoRepository.buscarTodos({ ciclistaId: ciclistaId })

        if(cartaoDeCredito.length === 0){
            return response.status(404).json({mensagem: "Cartão de crédito não encontrado", codigo: 404});
        }

        const atualizado = cartaoDeCreditoRepository.atualizar(cartaoDeCredito[0].getId(), cartaoAtualizado);

        return response.status(200).json(atualizado);
        
    }

}