require('dotenv').config();

const Ciclista = require("../entities/ciclista.entity");
module.exports = class CiclistaController {

    constructor( { datasource, axiosService } ) {
        // Não esqueça de passar qualquer dependência via construtor da classe...
        this.datasource = datasource;
        this.axiosService = axiosService;
    }

    getCiclistaById = (request, response) => {
        const id = request.params.idCiclista;
        
        if(!id){
            return response.status(422).json({mensagem: "Id inválido", codigo: 422});
        }

        const ciclistaRepository = this.datasource.getRepository("ciclista")

        const ciclista = ciclistaRepository.buscarPorId(id);

        if(!ciclista){
            return response.status(404).json({mensagem: "Ciclista não encontrado", codigo: 404});
        }

        return response.status(200).json(ciclista.getDados());
    }

    postCiclista = async (request, response) => {
        const ciclista = request.body.ciclista;
        const meioDePagamento = request.body.meioDePagamento

        const { nome, nascimento, cpf, passaporte, nacionalidade, email, urlFotoDocumento, senha } = ciclista;
        const { numero, validade, nomeTitular, cvv } = meioDePagamento;

        const brasileiro = nacionalidade.toLowerCase() === "brasileiro";

        if (!this.#validarDadosDoCiclista(brasileiro, nome, nascimento, cpf, nacionalidade, email, passaporte, urlFotoDocumento, ciclista)) {
            return response.status(422).json({mensagem: "Dados inválidos", codigo: 422});
        }

        if (!senha) {
            return response.status(422).json({ mensagem: "A senha deve ser informada.", codigo: 422 });
        } 
        
        if (!meioDePagamento || !numero || !validade || !nomeTitular || !cvv) {
            return response.status(422).json({ mensagem: "O meio de pagamento deve ser informado.", codigo: 422 });
        }

        const emailRegex = /^[^\s@]+@[^\s@]+$/;

        if (!emailRegex.test(email)) {
            return response.status(422).json({ mensagem: "Email inválido", codigo: 422 });
        }

        const ciclistaRepository = this.datasource.getRepository("ciclista")
        
        const emailExiste = await ciclistaRepository.buscarTodos({email: email});

        if(emailExiste.length > 0){
            return response.status(422).json({mensagem: "Email já cadastrado", codigo: 422});
        };

        const cartaoValidado = await this.axiosService.post(`${process.env.EXTERNO_URL}/validaCartaoDeCredito`, meioDePagamento);

        if (!cartaoValidado.success) {
            return response.status(422).json([{ mensagem: "Cartão Reprovado", codigo: 422 }]);
        }

        const ciclistaDados = { ...ciclista, status: Ciclista.Status.AGUARDANDO_CONFIRMACAO, dataAtivacao: null, bicicleta: null };

        let ciclistaSalvo = null;
        
        try {
            ciclistaSalvo = await ciclistaRepository.salvar(ciclistaDados);
        } catch (error) {
            console.log(error);
           return response.status(422).json([{ mensagem: "Erro ao salvar ciclista", codigo: 422 }]);
        }

        const meioDePagamentoRepository = this.datasource.getRepository("cartaoDeCredito")
        
        const meioDepagamentoCiclistaId = {
            ...meioDePagamento,
            ciclistaId: ciclistaSalvo.getId()
        }
        
        try {
            await meioDePagamentoRepository.salvar(meioDepagamentoCiclistaId);
        } catch (error) {
            return response.status(422).json([{ mensagem: "Erro ao salvar cartão de crédito", codigo: 422 }]);
        }
        
        const emailObject = {
            email: email,
            assunto: "Confirmação de cadastro",
            mensagem: `Olá ${nome}, seu cadastro foi realizado com sucesso!`
        }
        
        const emailEnviado = await this.axiosService.post(`${process.env.EXTERNO_URL}/enviarEmail`, emailObject);
        
        if (!emailEnviado.success) {
            return response.status(422).json([{ mensagem: "Falha ao enviar o email para o ciclista.", codigo: 422 }]);
        }

        return response.status(200).json(ciclistaSalvo.getDados())
    }

    putCiclista = async (request, response) => {
        const ciclista = request.body;
        const id = parseInt(request.params.idCiclista, 10);

        const { nome, nascimento, cpf, passaporte, nacionalidade, email, urlFotoDocumento } = ciclista;

        const brasileiro = nacionalidade.toLowerCase() === "brasileiro";

        if (!this.#validarDadosDoCiclista(brasileiro, nome, nascimento, cpf, nacionalidade, email, passaporte, urlFotoDocumento, ciclista)) {
            return response.status(422).json({mensagem: "Dados inválidos", codigo: 422});
        }

        const ciclistaRepository = this.datasource.getRepository("ciclista");
        const emailRegex = /^[^\s@]+@[^\s@]+$/;


        if (email) {
            if (!emailRegex.test(email)) {
                return response.status(422).json({ mensagem: "Email inválido", codigo: 422 });
            }

            const emailExiste = await ciclistaRepository.buscarTodos({ email: email });

            if (emailExiste.length > 0) {
                return response.status(422).json({ mensagem: "Email já cadastrado", codigo: 422 });
            }
        }

        const emailEnviado = await this.axiosService.post(`${process.env.EXTERNO_URL}/enviarEmail`, { email, assunto: "Dados alterados", mensagem: "Os dados do ciclista foram alterados." });

        if (!emailEnviado.success) {
            return response.status(422).json([{ mensagem: "Falha ao enviar email para o ciclista.", codigo: 422 }]);
        }

        try {
            const ciclistaSalvo = await ciclistaRepository.atualizar(id, ciclista);
            return response.status(200).json(ciclistaSalvo.getDados())
        } catch (error) {
            return response.status(404).json({mensagem: "Ciclista não encontrado", codigo: 404});
        }

    }

    #validarDadosDoCiclista = (brasileiro, nome, nascimento, cpf, nacionalidade, email, passaporte, urlFotoDocumento, ciclista) => {
        if (brasileiro) {
            //popula passaporte com dados irrelevantes
            ciclista.passaporte = {
                numero: "000000",
                validade: "000000",
                pais: "Brasil"
            }
        } else {
            //popula cpf com dados irrelevantes
            ciclista.cpf = "00000000000";
        }

        if(
            !ciclista || 
            !nome ||
            !nascimento ||
            (brasileiro && !cpf) ||
            (!brasileiro && (
                !passaporte ||
                !ciclista.passaporte ||
                !ciclista.passaporte.numero ||
                !ciclista.passaporte.validade ||
                !ciclista.passaporte.pais
            )) ||
            !nacionalidade ||
            !email ||
            !urlFotoDocumento
        ){  
            return false;
        }

        return true;
    }

    postAtivarCiclista = async (request, response) => {
        const id = parseInt(request.params.idCiclista, 10);

        if(!id){
            return response.status(422).json({mensagem: "Dados inválidos", codigo: 422});
        }

        const ciclistaRepository = this.datasource.getRepository("ciclista")

        const ciclista = await ciclistaRepository.buscarPorId(id);
        
        if(!ciclista){
            return response.status(404).json({mensagem: "Ciclista não encontrado", codigo: 404});
        }
        
        ciclista.ativar();
        return response.status(200).json(ciclista.getDados())
    }

    getExisteEmail = async (request, response) => {
        const email = request.params.email;

        if(!email){
            return response.status(400).json({mensagem: "Email não enviado como parâmetro", codigo: 400});
        }

        const emailRegex = /^[^\s@]+@[^\s@]+$/;

        if (!emailRegex.test(email)) {
            return response.status(422).json({ mensagem: "Email inválido", codigo: 422 });
        }

        const ciclistaRepository = this.datasource.getRepository("ciclista")

        const ciclista = ciclistaRepository.buscarTodos({email: email});

        if(ciclista.length > 0){
            return response.status(200).json(true)
        }

        return response.status(200).json(false)
    }
}