require("dotenv").config()

module.exports = class AluguelController {
    #datasource;
    #axiosService; 
    
    constructor({ datasource, axiosService }) {
        this.#datasource = datasource;
        this.#axiosService = axiosService;
    }

    alugarBicicleta = async (req, res) => {
        let { ciclista, trancaInicio } = req.body;

        if (ciclista === undefined || trancaInicio === undefined) {
            return res.status(422).json([{ codigo: 422, mensagem: "Deve ser informado o ciclista e a tranca de inicio do aluguel." }]); 
        }

        ciclista = Number.parseInt(ciclista);
        trancaInicio = Number.parseInt(trancaInicio);

        const tranca = await this.#tranca(trancaInicio);

        if (tranca === null) {
            return res.status(422).json([{ codigo: 422, mensagem: "Número da tranca inválido." }]);
        }

        const resultado = await this.#bicicletaDaTranca(trancaInicio);

        if (resultado === null) {
            return res.status(422).json([{ codigo: 422, mensagem: "Não existe bibicleta associada à tranca." }]);
        }

        const [bicicleta] = resultado;

        if (bicicleta.status !== "DISPONIVEL") {
            return res.status(422).json([{ codigo: 422, mensagem: "Bicicleta não está disponível para o aluguel." }]);
        }

        if (tranca.status !== "OCUPADA") {
            return res.status(422).json([{ codigo: 422, mensagem: "Tranca deve estar ocupada para fazer um aluguel." }]);
        }

        const ciclistaRepository = this.#datasource.getRepository("ciclista");

        const ciclistaCadastrado = ciclistaRepository.buscarPorId(ciclista);
        
        if (ciclistaCadastrado === null) {
            return res.status(422).json([{ codigo: 422, mensagem: "Ciclista não encontrado." }]);
        }
        
        if (!ciclistaCadastrado.podeAlugarBicicleta()) {
            await ciclistaCadastrado.enviarEmail(this.#axiosService, "Voce já se encontra em um aluguel, camarada...", "dados do aluguel aqui...");
            return res.status(422).json([{ codigo: 422, mensagem: "Ciclista já se encontra em um aluguel." }]); 
        }

        if (bicicleta.status === "EM_REPARO") {
            return res.status(422).json([{ codigo: 422, mensagem: "Bicicleta não está em condição de uso." }]);
        }

        if (this.#enviarCobranca(ciclista) === null) {
            return res.status(422).json([{ codigo: 422, mensagem: "O pagamento não foi concluído, mas a cobrança foi registrada para ser cobrada posteriormente." }]);
        }

        const aluguelRepository = this.#datasource.getRepository("aluguel");

        const dataRetirada = new Date();

        const aluguel = await aluguelRepository.salvar({ ciclista, bicicleta: bicicleta.id, trancaInicio, horaInicio: dataRetirada, cobranca: 10, trancaFim: -1, horaFim: "", status: "EM_ANDAMENTO" });
        await this.#axiosService.post(`${process.env.EQUIPAMENTO_URL}/bicicleta/${bicicleta.id}/status/EM_USO`);
        await this.#axiosService.post(`${process.env.EQUIPAMENTO_URL}/tranca/${trancaInicio}/destrancar`);
        await ciclistaCadastrado.enviarEmail(this.#axiosService, "Bicicleta alugada", {"Horario da Retirada": dataRetirada, Traca: tranca.id, valor: 10 });

        return res.status(200).json(aluguel.getDados());
    }

    devolverBicicleta = async (req, res) => {
        let { idTranca, idBicicleta } = req.body;

        if (idTranca === undefined || idBicicleta === undefined) {
            return res.status(422).json([{ codigo: 422, mensagem: "Deve ser informado o id da tranca o id da bicicleta."}]);
        }

        idTranca = Number.parseInt(idTranca);
        idBicicleta = Number.parseInt(idBicicleta);
        
        const bicicleta = await this.#bicicleta(idBicicleta);
        
        if (bicicleta === null) {
            return res.status(422).json([{ codigo: 422, mensagem: "Número da bicicleta inválido." }]);
        }

        if (bicicleta.status !== "EM_USO") {
            return res.status(422).json([{ codigo: 422, mensagem: "Bicicleta deve estar em uso para ser devolvida." }]);
        }

        const tranca = await this.#tranca(idTranca);

        if (tranca === null) {
            return res.status(422).json([{ codigo: 422, mensagem: "Número da tranca inválido." }]);
        }

        if (tranca.status !== "DISPONIVEL") {
            return res.status(422).json([{ codigo: 422, mensagem: "Tranca deve estar disponível." }]);
        }

        if (bicicleta.status === "NOVA" || bicicleta.status === "EM_REPARO") {
            await this.#axiosService.post(`${process.env.EQUIPAMENTO_URL}/tranca/integrarNaRede`, { idTranca, idBicicleta, idFuncionario: -1 });
            return res.status(422).json([{ codigo: 422, mensagem: "Bicicleta precisa ser integrada na rede." }]);
        }

        const aluguelRepository = this.#datasource.getRepository("aluguel");

        const [aluguel] = aluguelRepository.buscarTodos({ bicicleta: idBicicleta, horaFim: "", trancaFim: -1 });
        if (aluguel === undefined) {
            return res.status(422).json([{ codigo: 422, mensagem: "Para efetuar uma devolução, antes é necessário realizar um aluguel." }]);
        }

        const tempoExcedido = aluguel.meiasHorasExcedidas();
        const valorExtra = 5*tempoExcedido;
        
        if (tempoExcedido > 0) {
           await aluguel.cobrarValorExtra(this.#axiosService, valorExtra);
        }

        const horaFim = new Date();
        aluguelRepository.atualizar(aluguel.getId(), { horaFim, trancaFim: idTranca, cobranca: aluguel.getCobranca() + valorExtra });
        
        const ciclistaRepository = this.#datasource.getRepository("ciclista");

        const ciclistaCadastrado = ciclistaRepository.buscarPorId(aluguel.getCiclista());

        await this.#axiosService.post(`${process.env.EQUIPAMENTO_URL}/bicicleta/${idBicicleta}/status/DISPONIVEL`);
        await this.#axiosService.post(`${process.env.EQUIPAMENTO_URL}/tranca/${idTranca}/trancar`);
        await ciclistaCadastrado.enviarEmail(this.#axiosService, "Bicicleta alugada", aluguel.dadosDaDevolucao() );

        return res.status(200).json(aluguel.getDados());
    }

    #tranca = async (id) => {
        return this.#respostaTratada(await this.#axiosService.get(`${process.env.EQUIPAMENTO_URL}/tranca/${id}`));
    }

    #bicicleta = async (id) => {
        return this.#respostaTratada(await this.#axiosService.get(`${process.env.EQUIPAMENTO_URL}/bicicleta/${id}`));
    }

    #bicicletaDaTranca = async (id) => {
        return this.#respostaTratada(await this.#axiosService.get(`${process.env.EQUIPAMENTO_URL}/tranca/${id}/bicicleta`));
    }

    #enviarCobranca = async (ciclista) => {
        const response = await this.#axiosService.post(`${process.env.EXTERNO_URL}/cobranca`, { valor: 10, ciclista });

        if (!response.success) {
            await this.#axiosService.post(`${process.env.EXTERNO_URL}/filaCobranca`, { valor: 10, ciclista });
            return null;
        }
    }

    #respostaTratada = (response) => {
        if (!response.success) {
            return null;
        }

        return response.data;
    }

}