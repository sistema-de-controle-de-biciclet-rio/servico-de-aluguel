const { readdir } = require("fs/promises");

const DataSource = {
    tabelas: {},
    initialize: async (source = []) => {
        let files = [];

        if (source.length === 0) {
            files = await readdir("./src/entities");
        } else {
            files = source;
        }

        for (const file of files) {
            const model = file.split(".")[0];

            if (model === "index") {
                continue;
            }

            DataSource.tabelas[model] = []
        }
    },
    clear: async () => {
        for (const tabela in DataSource.tabelas) {
            DataSource.tabelas[tabela] = [];

            const module = await import(`./entities/${tabela}.entity.js`);
            const { default: myDefault } = module;
            myDefault[tabela + "Id"] = 0;
        }
    },
    getRepository: (tabela) => {
        return {
            salvar: async (dados) => {
                const module = await import(`./entities/${tabela}.entity.js`);

                const { default: myDefault } = module;

                const model = new myDefault(dados);

                for (const dado in dados) {
                    if (model[`_${dado}`] === undefined) {
                        throw Error(`Propriedade ${dado} não existe em ${tabela}.`);
                    }
                }

                const propriedades = Object.getOwnPropertyNames(model)
                    .filter(propriedade => {
                        return propriedade !== "_id" && typeof model[propriedade] !== "function";
                    })
                    .map(propriedade => {
                        return propriedade.substring(1);
                    });

                for (const propriedade of propriedades) {
                    if (!(propriedade in dados)) {
                        throw Error(`Propriedade faltando: ${propriedade}.`);
                    }
                }

                DataSource.tabelas[tabela].push(model);

                return model;
            },
            buscarTodos: (criterios = {}) => {
                const entidades = DataSource.tabelas[tabela];

                if (Object.keys(criterios).length === 0) {
                    return entidades;
                }

                const filtro = entidades.filter(entidade => {
                    for (const criterio in criterios) {
                        if (!(`_${criterio}` in entidade)) {
                            throw Error(`Propriedade ${criterio} não existe em ${tabela}.`);
                        }

                        if (entidade[`_${criterio}`] !== criterios[criterio]) {
                            return false;
                        }
                    }

                    return true;
                });

                return filtro;
            },
            buscarPorId: (id) => {
                const idNumerico = Number.parseInt(id);

                const entidade = DataSource.tabelas[tabela].filter(elemento => {
                    return elemento._id === idNumerico;
                });

                if (entidade.length === 0) return null;

                return entidade[0];
            },

            atualizar(id, payload) {
                const entidade = this.buscarPorId(id);

                if (entidade === null) throw Error("Entidade não encontrada.");
                
                const propriedades = Object.getOwnPropertyNames(entidade)
                    .filter(propriedade => {
                        return propriedade !== "_id" && typeof entidade[propriedade] !== "function";
                    })
                    .map(propriedade => {
                        return propriedade.substring(1);
                    });

                for (const key in payload) {
                    let propriedadeExiste = false;

                    for (const prop of propriedades) {
                        if (prop === key) {
                            propriedadeExiste = true;
                        }
                    }

                    if (!propriedadeExiste) {
                        throw Error(`Propriedade inválida: ${key}`);
                    }

                    entidade["_" + key] = payload[key];
                }

                return entidade;
            },
            deletar: (id) => {
                const idNumerico = Number.parseInt(id);

                const entidade = DataSource.tabelas[tabela].filter(elemento => {
                    return elemento._id === idNumerico;
                });

                if (entidade.length === 0) return null;

                const index = DataSource.tabelas[tabela].indexOf(entidade[0]);

                DataSource.tabelas[tabela].splice(index, 1);

                return entidade[0];
            }
        }
    }
}

DataSource.initialize()
    .then(() => {
        console.log("Datasource inicializado.");
    })
    .catch(err => {
        console.error(err);
    });


module.exports = DataSource;
