const express = require("express");
const { ciclistaRoutes, cartaoDeCreditoRoutes, funcionarioRoutes, aluguelRoutes } = require("./routes/index.js");
const DataSource = require("./datasource.js");
const populate = require("./populate.js");


const app = express();

app.disable('x-powered-by');
app.use(express.json());
app.use(ciclistaRoutes);
app.use(cartaoDeCreditoRoutes);
app.use(funcionarioRoutes);
app.use(aluguelRoutes);
app.get("/restaurarDados", async (req, res) => {
    try {
        await DataSource.clear();
        await populate();
    } catch (err) {
        return res.status(500).json({ mensagem: err.message });
    }

    return res.status(200).end();
});

module.exports = app;