const ciclistaRoutes = require("./ciclista.routes.js");
const cartaoDeCreditoRoutes = require("./cartaoDeCredito.routes.js");
const funcionarioRoutes = require("./funcionario.routes.js");
const aluguelRoutes = require("./aluguel.routes.js");

module.exports = { 
    ciclistaRoutes,
    cartaoDeCreditoRoutes,
    funcionarioRoutes,
    aluguelRoutes
}