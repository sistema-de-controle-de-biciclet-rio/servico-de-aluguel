const express = require("express");
const router = express.Router();
const { CiclistaController } = require("../controllers/index.js");
const datasource = require("../datasource.js");
const AxiosService = require("../services/axios.service.js");
const { default: axios } = require("axios");

const axiosService = new AxiosService(axios);

const controller = new CiclistaController({ datasource, axiosService });

router.post("/ciclista", controller.postCiclista);
router.get("/ciclista/:idCiclista", controller.getCiclistaById);
router.put("/ciclista/:idCiclista", controller.putCiclista);
router.post("/ciclista/:idCiclista/ativar", controller.postAtivarCiclista);
router.get("/ciclista/existeEmail/:email", controller.getExisteEmail);

//get /ciclista/:idCiclista/permiteAluguel
//get /ciclista/:idCiclista/biclicletaAlugada

// /devolucao


module.exports = router;
