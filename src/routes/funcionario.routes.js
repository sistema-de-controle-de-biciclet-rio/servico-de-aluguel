const express = require("express");
const router = express.Router();
const { FuncionarioController } = require("../controllers/index.js");
const datasource = require("../datasource.js");

// crie suas rotas aqui.

const controller = new FuncionarioController({ datasource });

router.post("/funcionario", controller.postFuncionario);
router.get("/funcionario/:idFuncionario", controller.getFuncionarioById);
router.get("/funcionario", controller.getAllFuncionarios);
router.put("/funcionario/:idFuncionario", controller.putFuncionario);
router.delete("/funcionario/:idFuncionario", controller.deleteFuncionario);

module.exports = router;
