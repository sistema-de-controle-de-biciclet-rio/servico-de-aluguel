
const express = require("express");
const router = express.Router();
const { AluguelController } = require("../controllers/index.js");
const datasource = require("../datasource.js");
const AxiosService = require("../services/axios.service.js");
const { default: axios } = require("axios");

const axiosService = new AxiosService(axios);

const controller = new AluguelController({ datasource, axiosService });

router.post("/aluguel", controller.alugarBicicleta);
router.post("/devolucao", controller.devolverBicicleta);

module.exports = router;
