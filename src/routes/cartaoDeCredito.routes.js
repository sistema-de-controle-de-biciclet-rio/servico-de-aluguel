const express = require("express");
const router = express.Router();
const CartaoDeCreditoController = require("../controllers/cartaoDeCredito.controller.js");
const datasource = require("../datasource.js")

// crie suas rotas aqui.

const controller = new CartaoDeCreditoController({ datasource });

router.get("/cartaoDeCredito/:idCiclista", controller.getCartaoDeCreditoByCiclistaId);
router.put("/cartaoDeCredito/:idCiclista", controller.putCartaoDeCredito);


module.exports = router;
