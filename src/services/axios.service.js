
module.exports = class AxiosService {
    #service;
    #baseUrl;
    #options; 
    
    constructor(axios, dados = {}) {
        const { baseUrl = "", options = {}  } = dados;

        this.#service = axios;
        this.#baseUrl = baseUrl;
        this.#options = options;
    }

    get = async (url, options = {}) => {
        return this.request(url, { method: "GET", ...options  });
    }

    post = async (url, body, options = {}) => {
        return this.request(url, { data: body, method: "POST", ...options });
    }

    request = async (url, options) => {
        let result;
        try {
            const response = await this.#service({ url: this.#baseUrl + url, ...this.#options, ...options });
            result = { success: true, status: response.status, data: response?.data, message: "Requisição feita com sucesso" };
        } catch (err) {
            result = { success: false, message: err.message };
        }

        return result;
    }
}