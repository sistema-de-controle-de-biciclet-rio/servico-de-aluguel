FROM node:20.13

WORKDIR /home/node/app

COPY package.json package-lock.json .

RUN npm install

COPY . .

EXPOSE 3000

CMD ["node", "src/index.js"]