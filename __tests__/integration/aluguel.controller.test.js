const { default: axios } = require("axios");
const app = require("../../src/app");
const request = require("supertest");
const populate = require("../../src/populate");
require("dotenv").config();

describe("AluguelController", () => {

    beforeAll(() => {
        populate();
    });

    describe("POST /aluguel", () => {
        test("Deve efetuar um aluguel com sucesso.", async () => {
            // Arrange && Act
            const response = await request(app)
                .post("/aluguel")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ ciclista: 1, trancaInicio: 1 });           


            // Assert
            expect(response.status).toBe(200);
        }, 30000);

        test("Deve falhar se o número da tranca for inválido.", async () => {
            // Arrange && Act
            const response = await request(app)
                .post("/aluguel")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ ciclista: 1, trancaInicio: 1000 });

            // Assert
            expect(response.status).toBe(422);
            expect(response.body).toEqual([{ codigo: 422, mensagem: "Número da tranca inválido." }]);
        }, 30000);
    });

    describe("POST /devolucao", () => {
        test("Deve efetuar a devolução de uma bicicleta.", async () => {
            // Arrange && Act
            const response = await request(app)
                .post("/devolucao")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ idBicicleta: 1, idTranca: 1 });           

            // Assert
            expect(response.status).toBe(200);

        }, 30000);
        
        test("Deve falhar se o número da bicicleta for inválido.", async () => {
            // Arrange && Act
            const response = await request(app)
                .post("/devolucao")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ idBicicleta: 1000, idTranca: 1 });           

            // Assert
            expect(response.status).toBe(422);
            expect(response.body).toEqual([{ codigo: 422, mensagem: "Número da bicicleta inválido." }]);
        }, 30000);
    });
});