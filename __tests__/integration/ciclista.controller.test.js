require("dotenv").config();
const app = require("../../src/app");
const request = require("supertest");
const populate = require("../../src/populate");

describe("CiclistaController", () => {

    beforeAll(() => {
        populate();
    });

    const postCiclistaBody = {
        "ciclista": {
            "nome": "primeiro ciclista",
            "nascimento": "2024-07-30",
            "passaporte": {
                "numero": "123123123",
                "validade": "2024-07-30",
                "pais": "indonesia"
            },
            "nacionalidade": "indo",
            "email": "emailusuario@gmail.com",
            "urlFotoDocumento": "string",
            "senha": "senha123"
        },
        "meioDePagamento": {
            "nomeTitular": "primeiro Caaasda",
            "numero": "5110110012341204",
            "validade": "2028-07-30",
            "cvv": "627"
        }
    }

    describe("POST /ciclista", () => {

        test("Deve cadastrar ciclista com sucesso", async () => {
            // Arrange && Act
            const response = await request(app)
                .post("/ciclista")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send(postCiclistaBody);

            // Assert
            expect(response.status).toBe(200);
        });

        test("Deve retornar ERRO ao tentar cadastrar um ciclista com email já cadastrado", async () => {
            // Arrange && Act
            const response = await request(app)
                .post("/ciclista")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ ...postCiclistaBody, ciclista: { ...postCiclistaBody.ciclista, email: "user4@example.com" } });
            //email é email que ja existe no banco pela amostra

            // Assert
            expect(response.status).toBe(422);
        });

        
    });

    const putCiclistaBody = {
        "nome": "joao ciclista",
        "nascimento": "2024-07-30",
        "passaporte": {
            "numero": "123123123",
            "validade": "2024-07-30",
            "pais": "indonesia"
        },
        "nacionalidade": "indo",
        "email": "outro@gmail.com",
        "urlFotoDocumento": "string",
        "senha": "senha123"
    };

    describe("PUT /ciclista", () => {
        test("Deve atualizar ciclista com sucesso", async () => {
            // Arrange && Act
            const response = await request(app)
                .put("/ciclista/1")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send(putCiclistaBody);

            // Assert
            expect(response.status).toBe(200);
        });
    });

    const putCiclistaBodyErro = {
        "nome": "joao ciclista",
        "nascimento": "2024-07-30",
        "passaporte": {
            "numero": "123123123",
            "validade": "2024-07-30",
            "pais": "indonesia"
        },
        "nacionalidade": "indo",
        //faltando dados
    };

    describe("PUT /ciclista", () => {
        test("Deve retornar ERRO ao passar dados faltando ao atualizar ciclista", async () => {
            // Arrange && Act
            const response = await request(app)
                .put("/ciclista/1")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send(putCiclistaBodyErro);

            // Assert
            expect(response.status).toBe(422);
        });
    });

    describe("Ativar ciclista", () => {
        test("Deve ativar ciclista com sucesso", async () => {
            // Arrange && Act
            const response = await request(app)
                .post("/ciclista/1/ativar")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json");

            // Assert
            expect(response.status).toBe(200);
        });
    });
});