require("dotenv").config();
const app = require("../../src/app");
const request = require("supertest");
const populate = require("../../src/populate");

describe("FuncionarioController", () => {

    beforeAll(() => {
        populate();
    });

    // test("Deve cadastrar ciclista com sucesso", async () => {
    //     // Arrange && Act
    //     const response = await request(app)
    //         .post("/ciclista")
    //         .set("Content-Type", "application/json")
    //         .set("Accept", "application/json")
    //         .send(postCiclistaBody);

    //     // Assert
    //     expect(response.status).toBe(200);
    // });

    const postFuncionarioBody = {
        "senha": "string",
        "confirmacaoSenha": "string",
        "email": "segundo@example.com",
        "nome": "jose das couves",
        "idade": 18,
        "funcao": "reparador",
        "cpf": "123123123"
    }

    describe("POST funcionario", () => {
        test("Deve cadastrar funcionario com sucesso", async () => {
            // Arrange && Act
            const response = await request(app)
                .post("/funcionario")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send(postFuncionarioBody);

            // Assert
            expect(response.status).toBe(200);
        });

        test("Deve retornar ERRO ao tentar cadastrar um funcionario com email já cadastrado", async () => {
            // Arrange && Act
            const response = await request(app)
                .post("/funcionario")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ ...postFuncionarioBody, email: "employee@example.com" });

            // Assert
            expect(response.status).toBe(422);
        });

        test("Deve retornar ERRO ao confirmar senha errado", async () => {
            // Arrange && Act
            const response = await request(app)
                .post("/funcionario")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ ...postFuncionarioBody, confirmacaoSenha: "senhadiferente" });

            // Assert
            expect(response.status).toBe(422);
        });
    });

    const putFuncionarioBody = {
        "senha": "string",
        "confirmacaoSenha": "string",
        "email": "email@example.com",
        "nome": "fernando",
        "idade": 33,
        "funcao": "administrativo"
    };

    describe("PUT funcionario", () => {
        test("Deve atualizar funcionario com sucesso", async () => {
            // Arrange && Act
            const response = await request(app)
                .put("/funcionario/1")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send(putFuncionarioBody);

            // Assert
            expect(response.status).toBe(200);
        });

        const putFuncionarioEmailInvalido = {
            "senha": "string",
            "confirmacaoSenha": "string",
            "email": "segundoexample.com",
            "nome": "fernando",
            "idade": 33,
            "funcao": "administrativo"
        };

        test("Deve retornar ERRO ao tentar atualizar um funcionario com email inválido", async () => {
            // Arrange && Act
            const response = await request(app)
                .put("/funcionario/1")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send(putFuncionarioEmailInvalido);



            // Assert
            expect(response.status).toBe(422);
        });

        test("Deve retornar ERRO ao tentar atualizar um funcionario com email já cadastrado", async () => {
            // Arrange && Act
            const response = await request(app)
                .put("/funcionario/1")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ ...postFuncionarioBody, email: "employee@example.com" });

            // Assert
            expect(response.status).toBe(422);
        });

        test("Deve retornar ERRO ao confirmar senha errado", async () => {
            // Arrange && Act
            const response = await request(app)
                .put("/funcionario/1")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ ...putFuncionarioBody, confirmacaoSenha: "senhadiferente" });

            // Assert
            expect(response.status).toBe(422);
        });

        test("Deve retornar ERRO ao tentar atualizar um funcionario que não existe", async () => {
            // Arrange && Act
            const response = await request(app)
                .put("/funcionario/999")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json")
                .send({ ...putFuncionarioBody, email: "inexistente@email.com" });

            // Assert
            expect(response.status).toBe(404);
        });
    });

    describe("DELETE funcionario", () => {
        test("Deve deletar funcionario com sucesso", async () => {
            // Arrange && Act
            const response = await request(app)
                .delete("/funcionario/1")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json");

            // Assert
            expect(response.status).toBe(200);
        });

        test("Deve retornar ERRO ao tentar deletar um funcionario que não existe", async () => {
            // Arrange && Act
            const response = await request(app)
                .delete("/funcionario/999")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json");

            // Assert
            expect(response.status).toBe(404);
        });
    });

    describe("GET funcionario", () => {
        test("Deve retornar todos os funcionarios", async () => {
            // Arrange && Act
            const response = await request(app)
                .get("/funcionario")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json");

            // Assert
            expect(response.status).toBe(200);
        });

        test("Deve retornar um funcionario por id", async () => {
            // Arrange && Act
            const response = await request(app)
                .get("/funcionario/2")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json");

            // Assert
            expect(response.status).toBe(200);
        });

        test("Deve retornar ERRO ao tentar buscar um funcionario que não existe", async () => {
            // Arrange && Act
            const response = await request(app)
                .get("/funcionario/999")
                .set("Content-Type", "application/json")
                .set("Accept", "application/json");

            // Assert
            expect(response.status).toBe(404);
        });
    });
});