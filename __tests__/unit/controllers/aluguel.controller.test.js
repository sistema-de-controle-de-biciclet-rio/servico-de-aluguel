const { AluguelController } = require("../../../src/controllers");

describe("AluguelController", () => {
    let aluguelController;
    const datasource = {
        getRepository: jest.fn().mockReturnValue({
            salvar: jest.fn(),
            buscarPorId: jest.fn(),
            buscarTodos: jest.fn(),
            atualizar: jest.fn()
        })
    };
    const axiosService = {
        post: jest.fn(),
        get: jest.fn()
    };
    let req = {};
    const res = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis()
    };

    beforeAll(() => {
        aluguelController = new AluguelController({ datasource, axiosService })
    });

    beforeEach(() => {
        res.json.mockClear();
        res.status.mockClear();
    });
    
    describe("alugarBicicleta()", () => {
        test("Deve efetuar o aluguel de uma bicicleta com sucesso.", async () => {
            // Arrange
            req.body = {
                trancaInicio: 1,
                ciclista: 1
            };

            axiosService.get
                .mockResolvedValueOnce({ success: true, data: { status: "OCUPADA"} })
                .mockResolvedValueOnce({ success: true, data: [{ status: "DISPONIVEL", id: 1 }] });

            const ciclista = {
                enviarEmail: jest.fn(),
                podeAlugarBicicleta: jest.fn().mockReturnValue(true)
            }

            datasource.getRepository().buscarPorId.mockReturnValueOnce(ciclista);

            axiosService.post.mockResolvedValue({ success: true });

            datasource.getRepository().salvar.mockResolvedValueOnce({
                getDados: jest.fn()
            });

            // Act
            await aluguelController.alugarBicicleta(req, res);

            // Assert
            expect(res.status).toHaveBeenCalledWith(200);
            expect(res.status).not.toHaveBeenCalledWith(422);
            expect(ciclista.enviarEmail).toHaveBeenCalledTimes(1);
            expect(datasource.getRepository).toHaveBeenCalledWith("ciclista");
            expect(datasource.getRepository).toHaveBeenCalledWith("aluguel");
            expect(datasource.getRepository().salvar).toHaveBeenCalled();
        });
    });

    describe("devolverBicicleta()", () => {
        test("Deve efetuar a devolução de uma bicicleta com sucesso.", async () => {
            req.body = {
                idTranca: 1,
                idBicicleta: 1
            }

            axiosService.get.mockResolvedValueOnce({ success: true, data: {
                status: "EM_USO"
            }});

            axiosService.get.mockResolvedValueOnce({ success: true, data: {
                status: "DISPONIVEL"
            }});

            const aluguel = {
                meiasHorasExcedidas: jest.fn().mockReturnValue(1),
                cobrarValorExtra: jest.fn(),
                getId: jest.fn(),
                getCobranca: jest.fn(),
                getDados: jest.fn().mockReturnValue("aluguel"),
                dadosDaDevolucao: jest.fn(),
                getCiclista: jest.fn()
            }

            datasource.getRepository().buscarTodos.mockReturnValue([aluguel]);

            const ciclista = {
                enviarEmail: jest.fn()
            }
            datasource.getRepository().buscarPorId.mockReturnValueOnce(ciclista);

            // Act
            await aluguelController.devolverBicicleta(req, res);

            // Assert
            expect(res.status).toHaveBeenCalledWith(200);
            expect(res.status).not.toHaveBeenCalledWith(422);
            expect(res.json).toHaveBeenCalledWith("aluguel");
            expect(datasource.getRepository).toHaveBeenCalledWith("aluguel");
            expect(datasource.getRepository).toHaveBeenCalledWith("ciclista");
            expect(aluguel.meiasHorasExcedidas).toHaveBeenCalled();
            expect(aluguel.cobrarValorExtra).toHaveBeenCalled();
            expect(ciclista.enviarEmail).toHaveBeenCalled();
        });
    });
});