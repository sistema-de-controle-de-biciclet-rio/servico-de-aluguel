const { CiclistaController } = require("../../../src/controllers/index.js");
const { Ciclista } = require("../../../src/entities/index.js");

describe('Ciclista Controller', () => {
    test('Deve criar ciclista corretamente', async () => {
        const ciclistaBody = {
            nome: "string",
            nascimento: "2024-05-24",
            cpf: "52673763088",
            passaporte: {
                numero: "string",
                validade: "2024-05-24",
                pais: "AI"
            },
            nacionalidade: "brasileiro",
            email: "email@gmail.com",
            urlFotoDocumento: "string",
            senha: "string",
        };

        const meioDePagamentoBody = {
            "numero": "5110110012341205",
            "validade": "2024-08-30",
            "nomeTitular": "João da Silva",
            "cvv": "023"
        };

        const request = {
            body: {
                ciclista: ciclistaBody,
                meioDePagamento: meioDePagamentoBody
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const ciclistaInstance = new Ciclista(ciclistaBody);

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                salvar: jest.fn().mockReturnValue(ciclistaInstance),
                buscarTodos: jest.fn().mockReturnValue([]),
            })
        }

        const mockAxios = {
            post: jest.fn().mockResolvedValue({ success: true })
        }

        const ciclistaController = new CiclistaController({ datasource, axiosService: mockAxios });

        await ciclistaController.postCiclista(request, response);

        expect(response.status).toBeCalledWith(200);
        expect(response.json).toBeCalledWith(ciclistaInstance.getDados());

        expect(datasource.getRepository).toBeCalledWith("ciclista");
        expect(datasource.getRepository).toBeCalledWith("cartaoDeCredito");
        expect(datasource.getRepository("ciclista").salvar).toBeCalledWith({ ...ciclistaBody, bicicleta: null, status: "AGUARDANDO_CONFIRMACAO", dataAtivacao: null });
        expect(datasource.getRepository("ciclista").buscarTodos).toBeCalledWith({ email: ciclistaBody.email });
        expect(datasource.getRepository("cartaoDeCredito").salvar).toBeCalledWith({ ...meioDePagamentoBody, ciclistaId: ciclistaInstance.getId() });
    });

    test('Deve retornar erro 422 quando faltar dados (no caso, email)', async () => {
        const ciclistaBody = {
            nome: "string",
            nascimento: "2024-05-24",
            cpf: "52673763088",
            passaporte: {
                numero: "string",
                validade: "2024-05-24",
                pais: "AI"
            },
            nacionalidade: "string",
            //esta sem email!
        }

        const meioDePagamentoBody = {
            "numero": "5110110012341205",
            "validade": "2024-08-30",
            "nomeTitular": "João da Silva",
            "cvv": "023"
        };

        const request = {
            body: {
                ciclista: ciclistaBody,
                meioDePagamento: meioDePagamentoBody
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {}

        const ciclistaController = new CiclistaController({ datasource });

        await ciclistaController.postCiclista(request, response);

        expect(response.status).toBeCalledWith(422);
        expect(response.json).toBeCalledWith({ mensagem: "Dados inválidos", codigo: 422 });
    });

    test('Deve retornar erro 422 quando email não for um email válido (post ciclista)', async () => {
        const ciclistaBody = {
            nome: "string",
            nascimento: "2024-05-24",
            cpf: "52673763088",
            passaporte: {
                numero: "string",
                validade: "2024-05-24",
                pais: "AI"
            },
            nacionalidade: "string",
            email: "emailgmail.com",
            urlFotoDocumento: "string",
            senha: "string",
        }

        const meioDePagamentoBody = {
            "numero": "5110110012341205",
            "validade": "2024-08-30",
            "nomeTitular": "João da Silva",
            "cvv": "023"
        };

        const request = {
            body: {
                ciclista: ciclistaBody,
                meioDePagamento: meioDePagamentoBody
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {}

        const ciclistaController = new CiclistaController({ datasource });

        await ciclistaController.postCiclista(request, response);

        expect(response.status).toBeCalledWith(422);
        expect(response.json).toBeCalledWith({ mensagem: "Email inválido", codigo: 422 });

    });

    test('Deve gettar ciclista corretamente', async () => {
        const idCiclista = 1;

        const request = {
            params: {
                idCiclista
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const ciclistaInstance = new Ciclista({
            nome: "string",
            nascimento: "2024-05-24",
            cpf: "52673763088",
            passaporte: {
                numero: "string",
                validade: "2024-05-24",
                pais: "AI"
            },
            nacionalidade: "string",
            email: "email@gmail.com",
            urlFotoDocumento: "string",
            senha: "string"
        });

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarPorId: jest.fn().mockReturnValue(ciclistaInstance)
            })
        }

        const ciclistaController = new CiclistaController({ datasource });

        await ciclistaController.getCiclistaById(request, response);

        expect(response.status).toBeCalledWith(200);
        expect(response.json).toBeCalledWith(ciclistaInstance.getDados());

        expect(datasource.getRepository).toBeCalledWith("ciclista");
        expect(datasource.getRepository("ciclista").buscarPorId).toBeCalledWith(idCiclista);
    })

    test('Deve retornar erro 422 quando id for inválido', async () => {

        const request = {
            params: {}
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {}

        const ciclistaController = new CiclistaController({ datasource });

        await ciclistaController.getCiclistaById(request, response);

        expect(response.status).toBeCalledWith(422);
        expect(response.json).toBeCalledWith({ mensagem: "Id inválido", codigo: 422 });
    })

    test('Deve retornar erro 404 quando não encontrar ciclista', async () => {
        const idCiclista = 1;

        const request = {
            params: {
                idCiclista
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarPorId: jest.fn()
            })
        }

        const ciclistaController = new CiclistaController({ datasource });

        await ciclistaController.getCiclistaById(request, response);

        expect(response.status).toBeCalledWith(404);
        expect(response.json).toBeCalledWith({ mensagem: "Ciclista não encontrado", codigo: 404 });

        expect(datasource.getRepository).toBeCalledWith("ciclista");
        expect(datasource.getRepository("ciclista").buscarPorId).toBeCalledWith(idCiclista);
    })

    test('Deve retornar o ciclista atualizado', async () => {
        const idCiclista = 1;
        const ciclistaAtualizado = {
            nome: "string",
            nascimento: "2024-05-24",
            cpf: "52673763088",
            passaporte: {
                numero: "string",
                validade: "2024-05-24",
                pais: "AI"
            },
            nacionalidade: "string",
            email: "email@gmail.com",
            urlFotoDocumento: "string"
        }

        const request = {
            params: {
                idCiclista
            },
            body: ciclistaAtualizado
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const ciclistaInstance = new Ciclista(ciclistaAtualizado);

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarPorId: jest.fn().mockReturnValue(ciclistaInstance),
                atualizar: jest.fn().mockReturnValue(ciclistaInstance),
                buscarTodos: jest.fn().mockReturnValue([])
            })
        }

        const ciclistaController = new CiclistaController({ datasource, axiosService: { post: jest.fn().mockResolvedValue({success: true}) } });

        await ciclistaController.putCiclista(request, response);

        expect(response.status).toBeCalledWith(200);
        expect(response.json).toBeCalledWith(ciclistaInstance.getDados());

        expect(datasource.getRepository("ciclista").buscarTodos).toBeCalledWith({ email: ciclistaAtualizado.email });
        expect(datasource.getRepository).toBeCalledWith("ciclista");
        expect(datasource.getRepository("ciclista").atualizar).toBeCalledWith(idCiclista, ciclistaAtualizado);
    })

    test('Deve retornar erro 422 quando faltar dados (no caso, cpf)', async () => {
        const idCiclista = 1;
        const ciclistaAtualizado = {
            nome: "string",
            nascimento: "2024-05-24",
            //faltando cpf
            nacionalidade: "brasileiro",
            email: "email@gmail.com",
            urlFotoDocumento: "string"
        }

        const request = {
            params: {
                idCiclista
            },
            body: ciclistaAtualizado
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {}

        const ciclistaController = new CiclistaController({ datasource });

        await ciclistaController.putCiclista(request, response);

        expect(response.status).toBeCalledWith(422);
        expect(response.json).toBeCalledWith({ mensagem: "Dados inválidos", codigo: 422 });
    })

    test('deve retornar 404 quando não encontrar ciclista para atualizar', async () => {
        const idCiclista = null;
        const ciclistaAtualizado = {
            nome: "string",
            nascimento: "2024-05-24",
            cpf: "52673763088",
            passaporte: {
                numero: "string",
                validade: "2024-05-24",
                pais: "AI"
            },
            nacionalidade: "string",
            email: "email@gmail.com",
            urlFotoDocumento: "string"
        }

        const request = {
            params: {
                idCiclista
            },
            body: ciclistaAtualizado
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarPorId: jest.fn().mockReturnValue(Error("Entidade não encontrada.")),
                buscarTodos: jest.fn().mockReturnValue([]),
                atualizar: jest.fn().mockImplementation(() => {
                    throw Error("Ciclista não encontrado.");
                })
            })
        }

        const ciclistaController = new CiclistaController({ datasource, axiosService: { post: jest.fn().mockResolvedValue({ success: true }) } });

        await ciclistaController.putCiclista(request, response);

        expect(response.status).toBeCalledWith(404);
        expect(response.json).toBeCalledWith({ mensagem: "Ciclista não encontrado", codigo: 404 });

        expect(datasource.getRepository).toBeCalledWith("ciclista");
    })

    test('Deve ativar ciclista corretamente', async () => {
        const idCiclista = 1;

        const request = {
            params: {
                idCiclista
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const ciclistaInstance = new Ciclista({
            nome: "string",
            nascimento: "2024-05-24",
            cpf: "52673763088",
            passaporte: {
                numero: "string",
                validade: "2024-05-24",
                pais: "AI"
            },
            nacionalidade: "string",
            email: "email@gmail",
            urlFotoDocumento: "string",
            senha: "string"
        });

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarPorId: jest.fn().mockReturnValue(ciclistaInstance)
            })
        }

        const ciclistaController = new CiclistaController({ datasource });

        await ciclistaController.postAtivarCiclista(request, response);

        expect(datasource.getRepository).toBeCalledWith("ciclista");
        expect(datasource.getRepository("ciclista").buscarPorId).toBeCalledWith(idCiclista);

        expect(response.status).toBeCalledWith(200);
        expect(response.json).toBeCalledWith(ciclistaInstance.getDados());
    })

    test('Deve retornar erro 422 quando id for inválido', async () => {
        const request = {
            params: {}
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {}

        const ciclistaController = new CiclistaController({ datasource });

        await ciclistaController.postAtivarCiclista(request, response);

        expect(response.status).toBeCalledWith(422);
        expect(response.json).toBeCalledWith({ mensagem: "Dados inválidos", codigo: 422 });
    })

    test('Deve retornar erro 404 quando não encontrar ciclista', async () => {
        const idCiclista = 4;

        const request = {
            params: {
                idCiclista
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarPorId: jest.fn()
            })
        }

        const ciclistaController = new CiclistaController({ datasource });

        await ciclistaController.postAtivarCiclista(request, response);

        expect(datasource.getRepository).toBeCalledWith("ciclista");
        expect(datasource.getRepository("ciclista").buscarPorId).toBeCalledWith(idCiclista);

        expect(response.status).toBeCalledWith(404);
        expect(response.json).toBeCalledWith({ mensagem: "Ciclista não encontrado", codigo: 404 });
    })

    test('Deve retornar se email existe (sucesso)', async () => {
        const email = "string@email.com";

        const request = {
            params: {
                email
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const ciclista = new Ciclista({
            nome: "string",
            nascimento: "2024-05-24",
            cpf: "52673763088",
            passaporte: {
                numero: "string",
                validade: "2024-05-24",
                pais: "AI"
            },
            nacionalidade: "string",
            email,
            urlFotoDocumento: "string",
            senha: "string"
        });

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarTodos: jest.fn().mockReturnValue([{ ciclista }])
            })
        }

        const ciclistaController = new CiclistaController({ datasource });

        await ciclistaController.getExisteEmail(request, response);

        expect(datasource.getRepository).toBeCalledWith("ciclista");
        expect(datasource.getRepository("ciclista").buscarTodos).toBeCalledWith({ email });

        expect(response.status).toBeCalledWith(200);
        expect(response.json).toBeCalledWith(true);

    })

    test('Deve retornar se email existe (falha)', async () => {
        const email = "falha@email.com";

        const request = {
            params: {
                email
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarTodos: jest.fn().mockReturnValue([])
            })
        }

        const ciclistaController = new CiclistaController({ datasource });

        await ciclistaController.getExisteEmail(request, response);

        expect(datasource.getRepository).toBeCalledWith("ciclista");
        expect(datasource.getRepository("ciclista").buscarTodos).toBeCalledWith({ email });

        expect(response.status).toBeCalledWith(200);
        expect(response.json).toBeCalledWith(false);

    })

    test('Deve retornar erro 400 quando email não for enviado como parâmetro', async () => {
        const request = {
            params: {}
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const ciclistaController = new CiclistaController({});

        await ciclistaController.getExisteEmail(request, response);

        expect(response.status).toBeCalledWith(400);
        expect(response.json).toBeCalledWith({ mensagem: "Email não enviado como parâmetro", codigo: 400 });
    })

    test('Deve retornar erro 422 quando email não for um email válido (getExisteEmail)', async () => {
        const email = "falhaemail.com";

        const request = {
            params: {
                email
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const ciclistaController = new CiclistaController({});

        await ciclistaController.getExisteEmail(request, response);

        expect(response.status).toBeCalledWith(422);
        expect(response.json).toBeCalledWith({ mensagem: "Email inválido", codigo: 422 });
    })
})