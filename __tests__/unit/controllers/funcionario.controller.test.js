const { FuncionarioController } = require('../../../src/controllers');
const { Funcionario } = require('../../../src/entities');

describe('FuncionarioController', () => {
    test('deve criar funcionario corretamente', async () => {
        const funcionarioBody = {
            senha: "string",
            confirmacaoSenha: "string",
            email: "user@example.com",
            nome: "string",
            idade: 20,
            funcao: "reparador",
            cpf: "string"
        }

        const request = {
            body: funcionarioBody
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const funcionarioInstance = new Funcionario(funcionarioBody);

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                salvar: jest.fn().mockReturnValue(funcionarioInstance),
                buscarTodos: jest.fn().mockReturnValue([]),
                buscarPorId: jest.fn().mockReturnValue(null),
            })
        }
        
        const funcionarioController = new FuncionarioController({ datasource });

        await funcionarioController.postFuncionario(request, response);

        expect(datasource.getRepository).toHaveBeenCalledWith("funcionario");
        expect(datasource.getRepository("funcionario").salvar).toHaveBeenCalledWith(funcionarioBody);
        
        expect(response.status).toHaveBeenCalledWith(200);
        expect(response.json).toHaveBeenCalledWith(funcionarioInstance.getDados());

    });

    test('deve retornar erro 422 ao criar funcionario com dados inválidos (sem campo nome)', async () => {
        const funcionarioBody = {
            senha: "string",
            confirmacaoSenha: "string",
            email: "",
            // nome: "string",
            idade: 20,
            funcao: "administrativo",
            cpf: "string"
        }

        const request = {
            body: funcionarioBody
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }


        const datasource = {}

        const funcionarioController = new FuncionarioController({ datasource });

        await funcionarioController.postFuncionario(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith({ mensagem: "Dados inválidos", codigo: 422 });

    });

    test('deve gettar todos os funcionarios corretamente', async () => {

        const request = {}

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const funcionario1 = new Funcionario({
            senha: "string",
            confirmacaoSenha: "string",
            email: "user@example.com",
            nome: "string",
            idade: 20,
            funcao: "reparador",
            cpf: "string"
        });

        const funcionario2 = new Funcionario({
            senha: "outrasenha",
            confirmacaoSenha: "outrasenha",
            email: "novouser@example.com",
            nome: "carlos abreu",
            idade: 25,
            funcao: "reparador",
            cpf: "123123"
        });

        const arrayFuncionarios = [
            funcionario1,
            funcionario2
        ]

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarTodos: jest.fn().mockReturnValue(arrayFuncionarios),
            })
        }

        const funcionarioController = new FuncionarioController({ datasource });

        await funcionarioController.getAllFuncionarios(request, response);

        expect(datasource.getRepository).toHaveBeenCalledWith("funcionario");
        expect(datasource.getRepository("funcionario").buscarTodos).toHaveBeenCalled();

        expect(response.status).toHaveBeenCalledWith(200);
        expect(response.json).toHaveBeenCalledWith(arrayFuncionarios.map(funcionario => funcionario.getDados()));

    });

    test('deve gettar funcionario por id corretamente', async () => {
        const idFuncionario = 1;

        const request = {
            params: {
                idFuncionario
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const funcionarioInstance = new Funcionario({
            senha: "outrasenha",
            confirmacaoSenha: "outrasenha",
            email: "novouser@example.com",
            nome: "carlos abreu",
            idade: 25,
            funcao: "administrativo",
            cpf: "123123"
        });

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarPorId: jest.fn().mockReturnValue(funcionarioInstance),
                buscarTodos: jest.fn().mockReturnValue([])
            })
        }

        const funcionarioController = new FuncionarioController({ datasource });

        await funcionarioController.getFuncionarioById(request, response);

        expect(datasource.getRepository).toHaveBeenCalledWith("funcionario");
        expect(datasource.getRepository("funcionario").buscarPorId).toHaveBeenCalledWith(idFuncionario);

        expect(response.status).toHaveBeenCalledWith(200);
        expect(response.json).toHaveBeenCalledWith(funcionarioInstance.getDados());

    });


    test('deve retornar erro 422 ao gettar funcionario por id com id inválido', async () => {
        const idFuncionario = null;

        const request = {
            params: {
                idFuncionario
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {}

        const funcionarioController = new FuncionarioController({ datasource });

        await funcionarioController.getFuncionarioById(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith({ mensagem: "Dados inválidos", codigo: 422 });

    });

    test('deve retornar erro 404 ao gettar funcionario por id inexistente', async () => {
        const idFuncionario = 1;

        const request = {
            params: {
                idFuncionario
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarPorId: jest.fn().mockReturnValue(null),
                buscarTodos: jest.fn().mockReturnValue([])
            })
        }

        const funcionarioController = new FuncionarioController({ datasource });

        await funcionarioController.getFuncionarioById(request, response);

        expect(response.status).toHaveBeenCalledWith(404);
        expect(response.json).toHaveBeenCalledWith({ mensagem: "Não encontrado", codigo: 404 });
    });

    test('deve deletar funcionario corretamente', async () => {
        const idFuncionario = 1;

        const request = {
            params: {
                idFuncionario
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                deletar: jest.fn().mockReturnValue(true),
                buscarTodos: jest.fn().mockReturnValue([])
            })
        }

        const funcionarioController = new FuncionarioController({ datasource });

        await funcionarioController.deleteFuncionario(request, response);

        expect(datasource.getRepository).toHaveBeenCalledWith("funcionario");
        expect(datasource.getRepository("funcionario").deletar).toHaveBeenCalledWith(idFuncionario);

        expect(response.status).toHaveBeenCalledWith(200);
        expect(response.json).toHaveBeenCalledWith({ mensagem: "Funcionario deletado com sucesso." });

    });

    test('deve retornar erro 422 ao deletar funcionario com id inválido', async () => {
        const idFuncionario = null;

        const request = {
            params: {
                idFuncionario
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {}

        const funcionarioController = new FuncionarioController({ datasource });

        await funcionarioController.deleteFuncionario(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith({ mensagem: "Dados inválidos", codigo: 422 });
    });

    test('deve retornar erro 404 ao deletar funcionario inexistente', async () => {
        const idFuncionario = 999;

        const request = {
            params: {
                idFuncionario
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                deletar: jest.fn().mockReturnValue(null),
                buscarTodos: jest.fn().mockReturnValue([])
            })
        }

        const funcionarioController = new FuncionarioController({ datasource });

        await funcionarioController.deleteFuncionario(request, response);

        expect(response.status).toHaveBeenCalledWith(404);
        expect(response.json).toHaveBeenCalledWith({ mensagem: "Funcionario não encontrado", codigo: 404 });
    });

    test('deve retornar o funcionario atualizado', async () => {
        const idFuncionario = 1;

        const funcionarioAtualizado = {
            senha: "novasenha",
            confirmacaoSenha: "novasenha",
            email: "novo@example.com",
            nome: "novo nome",
            idade: 20,
            funcao: "reparador",
        }

        const request = {
            params: {
                idFuncionario
            },
            body: funcionarioAtualizado
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const funcionarioInstance = new Funcionario(funcionarioAtualizado);

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                atualizar: jest.fn().mockReturnValue(funcionarioInstance),
                buscarTodos: jest.fn().mockReturnValue([]),
                buscarPorId: jest.fn().mockReturnValue(funcionarioInstance),
            })
        }

        const funcionarioController = new FuncionarioController({ datasource });

        await funcionarioController.putFuncionario(request, response);

        expect(datasource.getRepository).toHaveBeenCalledWith("funcionario");
        expect(datasource.getRepository("funcionario").atualizar).toHaveBeenCalledWith(idFuncionario, funcionarioAtualizado);

        expect(response.status).toHaveBeenCalledWith(200);
        expect(response.json).toHaveBeenCalledWith(funcionarioInstance.getDados());

    });

    test('deve retornar erro 422 ao atualizar funcionario com dados inválidos (sem campo nome)', async () => {
        const idFuncionario = 1;

        const funcionarioAtualizado = {
            senha: "novasenha",
            confirmacaoSenha: "novasenha",
            email: "novo@example.com",
            //nome: "novo nome",
            idade: 20,
            funcao: "reparador",
            cpf: "string"
        }

        const request = {
            params: {
                idFuncionario
            },
            body: funcionarioAtualizado
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {}

        const funcionarioController = new FuncionarioController({ datasource });

        await funcionarioController.putFuncionario(request, response);

        expect(response.status).toHaveBeenCalledWith(422);
        expect(response.json).toHaveBeenCalledWith({ mensagem: "Dados inválidos", codigo: 422 });


    });

    test('deve retornar erro 404 ao atualizar funcionario inexistente', async () => {
        const idFuncionario = 999;

        const funcionarioAtualizado = {
            senha: "novasenha",
            confirmacaoSenha: "novasenha",
            email: "novo@example.com",
            nome: "novo nome",
            idade: 20,
            funcao: "reparador",
            cpf: "string"
        }

        const request = {
            params: {
                idFuncionario
            },
            body: funcionarioAtualizado
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarPorId: jest.fn().mockReturnValue(null),
                buscarTodos: jest.fn().mockReturnValue([]),
            })
        }

        const funcionarioController = new FuncionarioController({ datasource });

        await funcionarioController.putFuncionario(request, response);

        expect(response.status).toHaveBeenCalledWith(404);
        expect(response.json).toHaveBeenCalledWith({ mensagem: "Funcionario não encontrado", codigo: 404 });


    });

});