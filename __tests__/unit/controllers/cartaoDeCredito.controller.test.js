const { CartaoDeCreditoController } = require('../../../src/controllers/index.js');
const { CartaoDeCredito, Ciclista } = require('../../../src/entities/index.js');

describe('CartaoDeCredito Controller', () => {
    test('Deve gettar o cartao que tem ciclistaId', async () => {
        
        const id = 10;

        const request = {
            params: {
                idCiclista: id
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const cartaoInstance = new CartaoDeCredito({
            nomeTitular: "string",
            numero: "40285701769787451404708",
            validade: "2024-05-24",
            cvv: "4904",
            ciclistaId: id
        });

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarTodos: jest.fn().mockReturnValue([ cartaoInstance ])
            }),
        }

        const cartaoDeCreditoController = new CartaoDeCreditoController({ datasource });

        cartaoDeCreditoController.getCartaoDeCreditoByCiclistaId(request, response);

        expect(response.status).toBeCalledWith(200);
        expect(response.json).toBeCalledWith([cartaoInstance.getDados()]);
        expect(datasource.getRepository).toBeCalledWith("cartaoDeCredito");
        expect(datasource.getRepository("cartaoDeCredito").buscarTodos).toBeCalledWith({ ciclistaId: id });
    })

    test('deve retornar 422 se id for invalido', async () => {
        const request = {
            params: {}
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {}

        const cartaoDeCreditoController = new CartaoDeCreditoController({ datasource });

        cartaoDeCreditoController.getCartaoDeCreditoByCiclistaId(request, response);

        expect(response.status).toBeCalledWith(422);
        expect(response.json).toBeCalledWith({mensagem: "Id inválido", codigo: 422});
    });

    test('get deve retornar 404 se nao achar cartao de credito', async () => {
        const id = 10;

        const request = {
            params: {
                idCiclista: id
            }
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarTodos: jest.fn().mockReturnValue([])
            }),
        }

        const cartaoDeCreditoController = new CartaoDeCreditoController({ datasource });

        cartaoDeCreditoController.getCartaoDeCreditoByCiclistaId(request, response);

        expect(response.status).toBeCalledWith(404);
        expect(response.json).toBeCalledWith({mensagem: "Cartão de crédito não encontrado", codigo: 404});
        expect(datasource.getRepository).toBeCalledWith("cartaoDeCredito");
        expect(datasource.getRepository("cartaoDeCredito").buscarTodos).toBeCalledWith({ ciclistaId: id });
    });

    test('deve atualizar o cartao de credito', async () => {
        const idCiclista = 10;

        const cartaoAtualizado = {
            nomeTitular: "string",
            numero: "40285701769787451404708",
            validade: "2024-05-24",
            cvv: "4904"
        };

        const request = {
            params: {
                idCiclista: idCiclista
            },
            body: cartaoAtualizado
        };

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        };

        // Mock do ciclista com getStatus
        const ciclistaMock = {
            getStatus: jest.fn().mockReturnValue(Ciclista.Status.ATIVO)
        };

        const cartaoInstance = new CartaoDeCredito({
            ...cartaoAtualizado,
            ciclistaId: idCiclista
        });

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarTodos: jest.fn().mockReturnValue([cartaoInstance]),
                atualizar: jest.fn().mockReturnValue(cartaoInstance),
                buscarPorId: jest.fn().mockReturnValue(ciclistaMock),  // Retorna o mock do ciclista
            }),
        };

        const cartaoDeCreditoController = new CartaoDeCreditoController({ datasource });

        cartaoDeCreditoController.putCartaoDeCredito(request, response);

        expect(response.status).toBeCalledWith(200);

        expect(datasource.getRepository).toBeCalledWith("cartaoDeCredito");
        expect(datasource.getRepository("cartaoDeCredito").buscarTodos).toBeCalledWith({ ciclistaId: idCiclista });
        expect(datasource.getRepository).toBeCalledWith("ciclista");
        expect(datasource.getRepository("ciclista").buscarPorId).toBeCalledWith(idCiclista);
        expect(ciclistaMock.getStatus).toBeCalled();  // Verifica se getStatus foi chamado
        expect(datasource.getRepository("cartaoDeCredito").atualizar).toBeCalledWith(cartaoInstance.getId(), cartaoAtualizado);
    });

    test('deve retornar erro ao tentar atualizar o cartão de crédito de um ciclista com status AGUARDANDO_CONFIRMACAO', async () => {

        const idCiclista = 10;

        const cartaoAtualizado = {
            nomeTitular: "string",
            numero: "40285701769787451404708",
            validade: "2024-05-24",
            cvv: "4904"
        };

        const request = {
            params: {
                idCiclista: idCiclista
            },
            body: cartaoAtualizado
        };

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        };

        const cartaoInstance = new CartaoDeCredito({
            ...cartaoAtualizado,
            ciclistaId: idCiclista
        });

        const ciclistaInstance = {
            getStatus: jest.fn().mockReturnValue(Ciclista.Status.AGUARDANDO_CONFIRMACAO)
        };

        const datasource = {
            getRepository: jest.fn().mockReturnValue({
                buscarTodos: jest.fn().mockReturnValue([cartaoInstance]),
                atualizar: jest.fn(),
                buscarPorId: jest.fn().mockReturnValue(ciclistaInstance),
            }),
        };

        const cartaoDeCreditoController = new CartaoDeCreditoController({ datasource });

        cartaoDeCreditoController.putCartaoDeCredito(request, response);

        expect(response.status).toBeCalledWith(403);
        expect(response.json).toBeCalledWith({ mensagem: "Ciclista inativo", codigo: 403 });

        expect(datasource.getRepository).toBeCalledWith("ciclista");
        expect(datasource.getRepository("ciclista").buscarPorId).toBeCalledWith(idCiclista);
        expect(ciclistaInstance.getStatus).toBeCalled();
    });

    test('put deve retornar 404 se id for invalido', async () => {
        const request = {
            params: {}
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {}

        const cartaoDeCreditoController = new CartaoDeCreditoController({ datasource });

        cartaoDeCreditoController.putCartaoDeCredito(request, response);

        expect(response.status).toBeCalledWith(404);
        expect(response.json).toBeCalledWith({mensagem: "Id inválido", codigo: 404});
    });

    test('put deve retornar 422 se dados forem invalidos', async () => {
        
        const idCiclista = 10;

        const cartaoAtualizado = {
            nomeTitular: "string",
            //faltando numero!
            validade: "2024-05-24",
            cvv: "4904"
        }
        
        const request = {
            params: {
                idCiclista: idCiclista
            },
            body: cartaoAtualizado
        }

        const response = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis()
        }

        const datasource = {}

        const cartaoDeCreditoController = new CartaoDeCreditoController({ datasource });

        cartaoDeCreditoController.putCartaoDeCredito(request, response);

        expect(response.status).toBeCalledWith(422);
        expect(response.json).toBeCalledWith({mensagem: "Dados inválidos", codigo: 422});
        
    });
});